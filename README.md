# Description
rc-app to write like an idiot
This rc-app will rewrite your own message on rocketchat

# Configuration
None

# Commands
/dau <mytext> - Write like an idiot

# Example
```
/dau this is crazy
AEH , TIHS IS CRAZY !!!!!!!!!!?!!!!?!!?!1
```

# Publish
```
rc-apps deploy --update
```

