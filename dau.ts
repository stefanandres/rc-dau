import {ISlashCommand, SlashCommandContext} from '@rocket.chat/apps-engine/definition/slashcommands';
import {IModify, IRead} from '@rocket.chat/apps-engine/definition/accessors';
import {App} from '@rocket.chat/apps-engine/definition/App';


let do_random = function(x) {
  return Math.floor(Math.random() * x) % x;
};

let repeat = function(x, y) {
  return Array(y + 1).join(x);
};

let switchchars = function(word) {
  // Switch neighbour Characters
  // e.g. "hello" becomes "ehllo"
  var chars, first, second, tmp;
  chars = word.split('');
  first = do_random(chars.length);
  if (first === (chars.length - 1)) {
    first = first - 1;
  }
  second = first + 1;
  // Swap characters
  tmp = chars[first];
  chars[first] = chars[second];
  chars[second] = tmp;
  return chars.join('');
};

let misspell = function(text) {
  // Misspell words 1/3 of the time
  var ret, word, _i, _len, _ref;
  ret = '';
  _ref = text.split(" ");
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    word = _ref[_i];
    if (do_random(3) === 0) {
      ret = ret + ' ' + switchchars(word);
    } else {
      ret = ret + ' ' + word;
    }
  }
  return ret;
};

let repeat_single_char = function(chars, rand) {
  var new_char, random_char, random_index;
  if (rand == null) {
    rand = 1;
  }
  random_index = do_random(chars.length);
  random_char = chars[random_index];
  new_char = repeat(random_char, 1 + do_random(rand));
  chars.splice(random_index, 0, new_char);
  return chars;
};

let eol = function(text) {
  // Add end-of-line exclamations
  var base, chars, rand, ret, x, _i;
  base = ' !?!?!?!1';
  chars = base.split('');
  for (x = _i = 1; _i <= 5; x = ++_i) {
    chars = repeat_single_char(chars, rand = 3);
  }
  ret = chars.join('');
  return text + ret;
};

let get_fillword = function() {
  var fillwords, ret;
  fillwords = ['EH', 'EEH', 'EHH', 'AEH', 'AEEH', 'AEEHHH'];
  ret = fillwords[do_random(fillwords.length)];
  return ret;
};

let stutter = function(text) {
  // Insert get_fillword() every 50% of time to the text
  var ret, word, _i, _len, _ref;
  ret = '';
  _ref = text.split(" ");
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    word = _ref[_i];
    if (do_random(2) === 0) {
      ret = ret + ' ' + get_fillword() + ' , ';
      }
    ret = ret + ' ' + word;
  }
  return ret;
};

let moron = function(text) {
  var chars, ret, word, _i, _len, _ref;
  ret = '';
  _ref = text.split(' ');
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    word = _ref[_i];
    if (do_random(2) === 0) {
      chars = word.split('');
      if (do_random(4) === 0) {
        ret = ret + ' ' + repeat_single_char(chars, 1).join('');
      }
      ret = ret + ' ' + word;
    } else {
      ret = ret + ' ' + word;
    }
  }
  return ret;
};

export class DauCommand implements ISlashCommand {
    public command = 'dau';
    public i18nDescription = 'Write like a dau';
    public i18nParamsExample = '';
    public providesPreview = false;

    constructor(private readonly app: App) {}

    public async executor(context: SlashCommandContext, read: IRead, modify: IModify): Promise<void> {
        const subcommand = context.getArguments().join(' ');
        let message = subcommand;

        const messageStructure = await modify.getCreator().startMessage();
        const sender = context.getSender(); // the user calling the slashcommand
        const room = context.getRoom(); // the current room

        // Add all idiot styles together
        //text = text.toUpperCase()
        //text = misspell(text)
        //text = moron(text)
        //text = stutter(text)
        //text = eol(text)

        //message = do_random(1).toString()
        message = message.toUpperCase()
        message = misspell(message)
        message = moron(message)
        message = stutter(message)
        message = eol(message)
        

        messageStructure
        .setSender(sender)
        .setRoom(room)
        .setText(message);

        await modify.getCreator().finish(messageStructure);
    }
}
